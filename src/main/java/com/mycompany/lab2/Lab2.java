/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author supha
 */
public class Lab2 {
    String table[] = new String[9];
    String T;
    int inputn;
    int b = 0;
    String winner = "None";
    String turnx = "X";
    String turno = "O";
    Scanner sn = new Scanner(System.in);



    static void printWelcome() {
        System.out.println("Welcome to OX Game");
    }  
    public void CreateNumTable(){
        for(int a = 0;a<= 8;a++){
            String as=String.valueOf(a+1);
            table[a] = as;
        }
    }
    public void PrintTable(){
        for(int a = 0;a<= 8;a++){
           b = b +1;
           if (b > 3){
               b = 1;
               System.out.println("");
           }
           System.out.print(table[a]+"  ");
           if ( a== 8){
               System.out.println("");
           }
    }
    }
    public void PrintTurn(int g){
        if(g == 1){
            T = turnx;
        }else if(g == 2){
            T = turno;
        }
        System.out.println(T+"'s Turn!");
    }
    public void InputNumber(){
        System.out.println("Please input number (1-9) : ");
        inputn = sn.nextInt();
        table[inputn-1] = T;
    }
    public void CheckWinner(){
        String case0,case1,case2,case3,case4,case5,case6,case7;
        case0 = table[0] + table[1] + table[2];
        case1 = table[3] + table[4] + table[5];
        case2 = table[6] + table[7] + table[8];
        case3 = table[0] + table[3] + table[6];
        case4 = table[1] + table[4] + table[7];
        case5 = table[2] + table[5] + table[8];
        case6 = table[0] + table[4] + table[8];
        case7 = table[2] + table[4] + table[6];

        if (case0.equals("XXX")){
                  winner = turnx;
            } else if (case0.equals("OOO")){
                winner = turno;
            }
              if (case1.equals("XXX")){
                  winner = turnx;
            } else if (case1.equals("OOO")){
                winner = turno;
            }
              if (case2.equals("XXX")){
                  winner = turnx;
            } else if (case2.equals("OOO")){
                winner = turno;
            }
              if (case3.equals("XXX")){
                  winner = turnx;
            } else if (case3.equals("OOO")){
                winner = turno;
            }
              if (case4.equals("XXX")){
                  winner = turnx;
            } else if (case4.equals("OOO")){
                winner = turno;
            }
              if (case5.equals("XXX")){
                  winner = turnx;
            } else if (case5.equals("OOO")){
                winner = turno;
            }
              if (case6.equals("XXX")){
                  winner = turnx;
            } else if (case6.equals("OOO")){
                winner = turno;
            }
              if (case7.equals("XXX")){
                  winner = turnx;
            } else if (case7.equals("OOO")){
                winner = turno;
            }
    }
    public void PrintWinner(){
            System.out.println("Player " + winner + " win the game!!!");
    }
    public static void main(String[] args) {
        Lab2 lb = new Lab2();
        lb.printWelcome();
        lb.CreateNumTable();
        
    }
}